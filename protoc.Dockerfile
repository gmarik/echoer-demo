FROM alpine as protoc_builder
RUN apk add --no-cache curl unzip

WORKDIR /protobuf

ENV PROTOC_VER=3.2.0
RUN curl -OL https://github.com/google/protobuf/releases/download/v${PROTOC_VER}/protoc-${PROTOC_VER}-linux-x86_64.zip && \
	unzip -o protoc-${PROTOC_VER}-linux-x86_64.zip -d /protobuf


FROM golang:1.11-alpine as go_builder
RUN apk add --no-cache curl unzip

WORKDIR /go/src/github.com/golang/

ENV GO_PROTO_VER=1.2.0
ENV GOPATH=/go
RUN curl -OL https://github.com/golang/protobuf/archive/v${GO_PROTO_VER}.zip && \
	unzip -o v${GO_PROTO_VER}.zip -d /go/src/github.com/golang/ && \
  mv /go/src/github.com/golang/protobuf-${GO_PROTO_VER} /go/src/github.com/golang/protobuf

RUN go install github.com/golang/protobuf/proto && \
	go install github.com/golang/protobuf/protoc-gen-go


FROM ubuntu

WORKDIR /

COPY --from=protoc_builder /protobuf /protobuf
COPY --from=go_builder /go/bin/proto* /usr/local/bin

ENTRYPOINT ["/protobuf/bin/protoc", "-I/protobuf"]
