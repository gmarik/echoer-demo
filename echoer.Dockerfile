FROM golang:1.11-alpine as go_builder
RUN apk add --no-cache curl unzip

WORKDIR /app

ENV GOFLAGS=-mod=vendor GOBIN=/app/bin CGO_ENABLED=0

COPY . /app/

RUN go install \
  gmarik/echoer/cmd/server \
  gmarik/echoer/cmd/client

FROM alpine

WORKDIR /app/

COPY --from=go_builder /app/bin/ /app/bin/
COPY *.pem /app/

ENTRYPOINT ["/app/bin/server"]