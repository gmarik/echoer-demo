package grpc_test

import (
	"context"
	"reflect"
	"testing"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gmarik/echoer/echoer"
	"gmarik/echoer/grpc"
	pb "gmarik/echoer/proto/echoer"
)

// ensures Server's interface conforms to the EchoService
var _ pb.EchoServiceServer = (*grpc.Server)(nil)

func TestServer_Echo(t *testing.T) {
	var (
		server = grpc.Server{
			Echoer: echoer.EchoerFunc(echoer.Echo),
		}
	)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	tests := []struct {
		desc    string
		have    *pb.EchoRequest
		want    *pb.EchoResponse
		wantErr error
	}{
		{
			desc:    "error: blank test",
			have:    &pb.EchoRequest{Text: ""},
			want:    nil,
			wantErr: status.Errorf(codes.InvalidArgument, "echoer: blank input: invalid"),
		},

		{
			desc: "echoes back",
			have: &pb.EchoRequest{Text: "hello world"},
			want: &pb.EchoResponse{Text: "hello world"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.desc, func(t *testing.T) {
			got, err := server.Echo(ctx, tt.have)

			if reflect.DeepEqual(tt.wantErr, err) == false {
				t.Errorf("Server.Echo() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Server.Echo() = %v, want %v", got, tt.want)
			}
		})
	}
}
