package grpc

import (
	"context"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"

	pb "gmarik/echoer/proto/echoer"
)

var (
	// Creds is a convenience alias
	Creds = grpc.Creds
)

// Echoer defines a interface/contract for echoing a string
type Echoer interface {
	Echo(string) (string, error)
}

// Server provides Echoer contract through GRPC
type Server struct {
	Echoer Echoer
}

// Echo is a echoing RPC
func (s *Server) Echo(ctx context.Context, req *pb.EchoRequest) (*pb.EchoResponse, error) {
	echo, err := s.Echoer.Echo(req.GetText())

	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "echoer: %s", err.Error())
	}

	return &pb.EchoResponse{
		Text: echo,
	}, nil
}

// ListenAndServe is a convenience wrapper around starting a listener and serving the connections
func (s *Server) ListenAndServe(ctx context.Context, addr string, opts ...grpc.ServerOption) error {
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	srv := grpc.NewServer(opts...)
	pb.RegisterEchoServiceServer(srv, s)

	go func() {
		<-ctx.Done()
		srv.GracefulStop()
	}()

	return srv.Serve(lis)
}

// WithTokenAuth is an example authentication with a token
func WithTokenAuth(token string) grpc.ServerOption {
	return grpc.UnaryInterceptor(func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {

		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, grpc.Errorf(codes.Unauthenticated, "no metadata in context")
		}

		if data, ok := md["auth-token"]; !(ok && len(data) > 0 && data[0] == token) {
			return nil, grpc.Errorf(codes.Unauthenticated, "oops")
		}

		return handler(ctx, req)
	})
}
