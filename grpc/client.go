package grpc

import (
	"context"

	"google.golang.org/grpc"

	pb "gmarik/echoer/proto/echoer"
)

var (
	// WithInsecure aliases the function from the grpc package
	WithInsecure = grpc.WithInsecure
	// WithTransportCredentials is a convenience alias
	WithTransportCredentials = grpc.WithTransportCredentials
	// WithPerRPCCredentials is a convenience alias
	WithPerRPCCredentials = grpc.WithPerRPCCredentials

	Dial = grpc.Dial
)

type Client struct {
	client pb.EchoServiceClient
}

func NewClient(conn *grpc.ClientConn) *Client {
	return &Client{client: pb.NewEchoServiceClient(conn)}
}

func (c *Client) Echo(ctx context.Context, text string) (string, error) {
	resp, err := c.client.Echo(ctx, &pb.EchoRequest{Text: text})
	if err != nil {
		return "", err
	}
	return resp.Text, nil
}
