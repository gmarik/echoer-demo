# Homework

## TLDR

Run:

1. `make build`: to build stuff
2. `make echo`: to run the echo demo

A demonstration of a system that prompts user with an input and echos the input back.
Behind the scenes the user input is fed to the client process that sends it over to the server using the GRPC protocol and echoes server response back to the user. 

## Prerequisites

- requires Docker on host machine: as things are built and run in containers
- requires internet access do download base images and build new

## Checklist

> The server must be written in Go

Check

> There must be an API contract between the clients and the server

I chose Protobuf + GRPC for contract definition & fulfilment.
Contract is described in `proto/echoer.proto`
Protocol compilation is done with `make proto` and GO bindings are generated in `proto/echoer/`.
Proto compilation is done in a docker image to not bloat the host environment.

> The server must have ability to communicate with clients simultaneously

The GRPC server is capable handling clients concurrently.


> The source must live in a code repository with access to history of commits

Check 

> There must be unit tests to cover at least the API contract on the server

Unit tests for GRPC contract are in `grpc/echoer_test.go` 


> You have a Makefile to easily build and demonstrate your server capabilities

- `make echo` is a demo entrypoint(requires `make build` first)
- `make build` builds docker image `gmarik/echoer`. The image contains the server and client binaries located at `/app/bin/{server,client}`
- `make test` runs tests
- `make proto` compiles the `proto/echoer.proto` and generates the GRPC stubs
- `make protoc` downloads the `protoc` compiler and go,grpc generators.

> You have good quality documentation as part of your code

Check?

> Communication between the client and the server is encrypted and authenticated

Check
- TLS encrypts communication between server and the client
- Basic token authentication is used to enable client access

## Notes

- the demo uses `go-1.11` and modules.
- the package name uses somewhat non-idiomatic name `gmarik/echoer` just for the sake of example. 
- the project structure follows the Domain Driven Design: each package fullfils requirements of a certain domain. IE `grpc` package implements `echoer.proto` protocol to expose `echoer` functionality to `grpc` clients.
`echoer` package provides core functionality of the service.


## Original Message

```
Hey there!  Thanks for your time meeting with us.

For the sake of the next round, we would like to take a peek at your code and there is no greater joy for everyone to gather around a piece of software that works and do something.  For that reason, we ask that you provide us a working service that does the following.

Write a server that echoes back whatever string a client sent through an API.

Requirements

The server must be written in Go
There must be an API contract between the clients and the server
The server must have ability to communicate with clients simultaneously
The source must live in a code repository with access to history of commits
There must be unit tests to cover at least the API contract on the server

Bonus

You have a Makefile to easily build and demonstrate your server capabilities
You have good quality documentation as part of your code
Communication between the client and the server is encrypted and authenticated
```