// Package echoer implements domain logic of echoing a text
// currently domain logic requirements are:
// 1. text is "echoed" back to the caller by returning a response containing the text
// 2. in case text is blank an error is returned
package echoer

import "github.com/pkg/errors"

// ErrInvalid is returned in case of an invalid argument
var ErrInvalid = errors.Errorf("invalid")

// Echo implements domain logic of echoing a text
func Echo(text string) (string, error) {
	if text == "" {
		return "", errors.Wrap(ErrInvalid, "blank input")
	}

	return text, nil
}

// EchoerFunc is a type that implements Echoer interface
type EchoerFunc func(text string) (string, error)

// Echo method fullfills the Echo interface
func (f EchoerFunc) Echo(text string) (string, error) {
	return f(text)
}
