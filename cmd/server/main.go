package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gmarik/echoer/echoer"
	"gmarik/echoer/grpc"

	"google.golang.org/grpc/credentials"
)

var (
	grpcAddr = flag.String("grpc-addr", ":5100", "grpc: listen address")

	//
	// TLS
	//
	serverKey  = flag.String("server-key", "ca_key.pem", "TLS: server key")
	serverCert = flag.String("server-cert", "ca_cert.pem", "TLS: server key")

	//
	// Auth
	//
	tokenAuthSecret = flag.String("token-auth-secret", "s3cr3t!", "token authentication secret")
)

func main() {
	flag.Parse()

	var (
		ctx, cancelFn = context.WithCancel(context.Background())
	)

	//
	// Ensure all processes shutdown
	//
	var wg sync.WaitGroup
	wg.Add(2)

	//
	// Echoer GRPC server
	//
	creds, err := credentials.NewServerTLSFromFile(*serverCert, *serverKey)
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		var server = grpc.Server{
			Echoer: echoer.EchoerFunc(echoer.Echo),
		}
		defer wg.Done()
		log.Printf("server=started address=%v\n", *grpcAddr)
		err := server.ListenAndServe(ctx, *grpcAddr,
			grpc.Creds(creds),
			grpc.WithTokenAuth(*tokenAuthSecret),
		)
		if err != nil {
			cancelFn()
		}
		log.Printf("server=stopped error=%v\n", err)
	}()

	//
	// Signals
	//
	go func() {
		defer wg.Done()
		sigc := make(chan os.Signal)
		signal.Notify(sigc, os.Interrupt, syscall.SIGTERM)

		select {
		case <-ctx.Done():
			return
		case sig := <-sigc:
			cancelFn()
			log.Printf("status=signal signal=%v\n", sig)
		}
	}()

	log.Println("status=started")
	wg.Wait()
	log.Println("status=stopped")
}
