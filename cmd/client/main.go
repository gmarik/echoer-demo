package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"google.golang.org/grpc/credentials"

	"gmarik/echoer/grpc"
)

var (
	grpcAddr = flag.String("grpc-addr", ":5100", "grpc: echo service address")

	//
	// TLS
	//
	serverCert = flag.String("server-cert", "ca_cert.pem", "grpc: echo service address")
	serverName = flag.String("server-name", "echoer", "TLS: set the server name as per certificate")

	//
	// Auth
	//
	tokenAuthSecret = flag.String("token-auth-secret", "s3cr3t!", "token authentication secret")
)

func main() {
	flag.Parse()

	var (
		ctx     = context.Background()
		scanner = bufio.NewScanner(os.Stdin)
	)

	creds, err := credentials.NewClientTLSFromFile(*serverCert, "")
	if err != nil {
		log.Fatal(err)
	}

	creds.OverrideServerName(*serverName)

	conn, err := grpc.Dial(*grpcAddr,
		grpc.WithTransportCredentials(creds),                           // TLS
		grpc.WithPerRPCCredentials(tokenAuth{Token: *tokenAuthSecret}), // client authentication
	)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := grpc.NewClient(conn)

	stat, err := os.Stdin.Stat()
	if err != nil {
		log.Fatal(err)
	}

	var isTTY = (stat.Mode() & os.ModeCharDevice) != 0

	for {
		if isTTY {
			fmt.Print("Say:")
		}
		if !scanner.Scan() {
			if err := scanner.Err(); err != nil {
				log.Fatal(err)
			}
			break
		}

		resp, err := client.Echo(ctx, scanner.Text())
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("Echoer:", resp)
	}
}

// tokenAuth is an example authentication with a token
type tokenAuth struct{ Token string }

func (a tokenAuth) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{"auth-token": a.Token}, nil
}

func (a tokenAuth) RequireTransportSecurity() bool { return true }
