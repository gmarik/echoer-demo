#!/bin/sh

# abort on errors
set -e

## demo script, that:
## 1. starts the echo server
## 2. runc the echo client cli feeding the input through the Stdin
## 3. shuts down the server

## starts the echoer server
SID=$(docker run -d gmarik/echoer)
## give the container a second to start
sleep 1

read -p "say:" INPUT

## run the client feeding the input through the standard input
## the input will be sent to the service and retuned back
echo $INPUT|docker run -i --rm --link ${SID}:echoer --entrypoint=/app/bin/client gmarik/echoer --grpc-addr="echoer:5100"
## cleanup
docker stop ${SID} > /dev/null