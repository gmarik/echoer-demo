#
# Demo: run a simple echo demo. 
# See README.md for the details
#
.PHONY: echo
echo:
	@ ./demo.sh

#
# runs tests
#
.PHONY: test
test:
	@docker run --rm -e CGO_ENABLED=0 -e GOFLAGS=-mod=vendor -w /app -v ${PWD}:/app golang:1.11-alpine go test gmarik/echoer/...

#
# Docker image: build client and the server binaries
#
.PHONY: test
build: proto test
	@docker build -t gmarik/echoer -f echoer.Dockerfile .

#
# Protobuf+GRPC
#
# generates protobuf and grpc stubs for the server
.PHONY: proto
proto: protoc
	@docker run --rm -v ${PWD}:/app gmarik/go-protoc -I/app/proto --go_out=plugins=grpc:/app/proto/echoer /app/proto/echoer.proto

# downloads protoc compiler and Go grpc stubs generator
.PHONY: protoc
protoc:
	@docker build -t gmarik/go-protoc -f protoc.Dockerfile ${PWD}

#
# self-signed certificates
#
certs:
	openssl req \
		-newkey rsa:2048 -nodes -keyout ca_key.pem \
		-days 365 -subj '/CN=echoer/emailAddress=admin@localhost/O=selfsigned' \
		-x509 -out ca_cert.pem

