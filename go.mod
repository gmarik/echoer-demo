module gmarik/echoer

require (
	github.com/golang/protobuf v1.2.0
	github.com/pkg/errors v0.8.0
	golang.org/x/net v0.0.0-20181113165502-88d92db4c548
	google.golang.org/grpc v1.16.0
)
